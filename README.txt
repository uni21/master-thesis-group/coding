This is the reference implementation accompanying my master's thesis. If you obtain this code via git, please run `fetch_libs.sh` to obtain all necessary libraries. In the version distributed with the thesis, this should already be done.

To build this project, execute `build.sh`. It should result in an executable named `thesis`. Run `thesis --help` to be informed about the usage.
