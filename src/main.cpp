#include <helper/stopwatch.hpp>
#include "helper/argument_parser.hpp"
#include "functionalities.hpp"
#include "tud/all_defs.hpp"
#include "graph_statistics.hpp"
using namespace thesis::literals;

int main(int argc, char **argv) {
    const auto args = helper::get_parsed(argc, argv);

    helper::Stopwatch watch;
    if (args.m_simple_kernel) {
        watch.lap();
        const auto input_db = thesis::read_to_tud_input(args);
        watch.print_lap("Reading took ");
        const auto kernel = thesis::calc_simple_kernel(args, input_db);
        watch.print_lap("Calculating gram took ");
        thesis::dump_simple_kernel(args, kernel);
        watch.print_total();
        return 0;
    }

    watch.lap();
    auto transformed = thesis::read_to_transformed(args);
    watch.print_lap("Reading and transforming took ");
    if (!args.m_transform_only) {
        thesis::calculate_minimal_times(args, transformed, watch);
    }
    thesis::dump_transformed(args, transformed);
    watch.print_total();
}
