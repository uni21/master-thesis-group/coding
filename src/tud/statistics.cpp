#include "tud/statistics.hpp"
#include "tud/graph_db.hpp"
#include "tud/temporal_node.hpp"
#include "tud/edge_data.hpp"

namespace tud {
    template<class Func>
    auto collect_results(const input_db_t &db, Func &&f) {
        using ret_t = std::invoke_result_t<Func, const input_db_t::graph_t &>;
        by_label_t<std::vector<ret_t>> ret;
        for (auto index = 0ul; index < db.size(); ++index) {
            ret[static_cast<unsigned long>(db.label(index))].push_back(f(db[index]));
        }
        return ret;
    }

    auto is_already_infected(const input_db_t::node_label_t &label, time_t time) {
        return label.by_time(time).m_label != 0;
    }

    auto get_infections_of_node(const input_db_t::graph_t::const_node_handle_t handle) -> infectiousness {
        infectiousness inf{};
        const auto start = handle->label().by_index(1ul).m_time;
        const auto edge_after = [start](const auto handle) { return handle->label().time >= start; };
        for (const auto edge_handle: handle->outgoing_edges() | nano::views::filter(edge_after)) {
            const auto &to_label = edge_handle->to()->label();
            const auto encounter_time_start = edge_handle->label().time;
            if (is_already_infected(to_label, encounter_time_start)) {
                continue;
            }
            ++inf.encounters;
            inf.infections += static_cast<size_t>(to_label.changes_at(encounter_time_start + time_t{1}));
        }
        return inf;
    }

    auto get_infectiousness(const input_db_t &db) -> by_label_t<std::vector<infectiousness>> {
        return collect_results(db, [&](const auto &graph) {
            infectiousness inf{};
            for (const auto node_handle: graph.nodes()) {
                if (node_handle->label().changes()) {
                    inf += get_infections_of_node(node_handle);
                }
            }
            return inf;
        });
    }

    auto get_infections(const input_db_t &db) -> by_label_t<std::vector<std::vector<time_t>>> {
        return collect_results(db, [&](const auto &graph) {
            std::vector<time_t> infections;
            for (const auto node_handle: graph.nodes()) {
                if (node_handle->label().changes()) {
                    infections.push_back(node_handle->label().by_index(1ul).m_time);
                }
            }
            return infections;
        });
    }
}  // namespace tud
