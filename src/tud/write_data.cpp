#include "tud/write_data.hpp"
#include "graph/common_defs.hpp"
#include "tud/graph_db.hpp"
#include "tud/temporal_node.hpp"
#include "tud/edge_data.hpp"
#include "helper/io.hpp"
#include "helper/misc.hpp"
#include <iterator>
#include <fstream>

namespace tud {
    using namespace graph::literals;
    using namespace helper;

    auto get_time(const EdgeData &data) {
        return data.time;
    }

    auto get_time(const thesis::time_t &t) {
        return t;
    }

    auto get_time(const thesis::time_t::UnderlyingType &t) {
        return t;
    }

    template<class DB>
    void _write_dataset(const fs::path &target, const DB &graph_db) {
        using loc_node_label_t = typename DB::node_label_t;
        if (!exists(target.parent_path())) {
            create_directories(target);
        }
        const auto filename_gen = filename_generator(target);
        std::ofstream adjacency_output = filename_gen(adjacency_appendix);
        std::ofstream graph_label_output = filename_gen(graph_label_appendix);
        std::ofstream graph_indicator_file = filename_gen(graph_indicator_appendix);
        std::ofstream edge_attributes_output = filename_gen(edge_attributes_appendix);
        std::ofstream node_labels_file = filename_gen(node_labels_appendix);

        auto graph_indicator_output = std::ostream_iterator<graph::edge_id_t>{graph_indicator_file, "\n"};
        auto node_labels_output = nano::ostream_iterator<loc_node_label_t>{node_labels_file, "\n"};

        auto base_node_index = 1_node;
        auto get_index = [&base_node_index](auto node_handle) { return node_handle.get_id() + base_node_index; };

        for (auto i = 0ul; i < graph_db.size(); ++i) {
            const auto &graph = graph_db[i];
            graph_label_output << graph_db.label(i) << '\n';
            std::fill_n(graph_indicator_output, graph.num_nodes().get(), graph::edge_id_t{i + 1});
            nano::transform(graph.nodes(), node_labels_output, [](const auto handle) { return handle->label(); });

            for (auto edge_handle: graph.edges()) {
                edge_attributes_output << get_time(edge_handle->label()) << '\n';
                auto [from, to] = edge_handle->apply_to_nodes(get_index);
                adjacency_output << from << ", " << to << '\n';
            }

            base_node_index += graph.num_nodes();
        }
    }

    constexpr char gram_index_sep = ':';
    constexpr char gram_entry_sep = ' ';

    template<class GramEntry>
    void write_gram_entry(std::ostream &ostream, size_t col_index, GramEntry entry) {
        if (entry) {
            ostream << col_index << gram_index_sep << entry << gram_entry_sep;
        }
    }

    void write_gram(const gram_t &gram, const input_db_t &db, std::ostream &ostream) {
        for (auto row = 0ul; row < db.size(); ++row) {
            ostream << db.label(row) << gram_entry_sep;
            write_gram_entry(ostream, 0, row + 1);

            const auto &gram_row = gram[row];
            for (auto col = 0ul; col < db.size(); ++col) {
                write_gram_entry(ostream, col + 1, gram_row[col]);
            }
            ostream << '\n';
        }
    }
    void write_feature_maps(const std::vector<feature_t> &fmap, std::ostream &ostream) {
        for (const auto &row: fmap) {
            for (const auto entry: row) {
                ostream << entry << ';';
            }
            ostream << '\n';
        }
    }
    void write_csv(std::string_view dataset_name, const fs::path &target, const transformed_db_t &graph_db) {
        constexpr std::string_view columns = "from,to,time";
        auto file_name = target / dataset_name;
        file_name += ".csv";

        std::ofstream output{file_name};
        output << columns << '\n';

        graph::node_id_t cur_start_id{};
        graph::node_id_t max_id;
        const auto write_node = [&](const auto handle) {
            output << (cur_start_id + handle.get_id()) << ',';
            update_max(max_id, cur_start_id);
        };
        for (const auto &graph: graph_db) {
            max_id = 0_node;
            for (const auto edge_handle: graph.edges()) {
                edge_handle->apply_to_nodes(write_node);
                output << edge_handle->label().time << '\n';
            }
            cur_start_id += max_id;
        }
    }
    void write_dataset(const fs::path &target, const transformed_db_t &graph_db) {
        _write_dataset(target, graph_db);
    }
    void write_dataset(const fs::path &target, const input_db_t &graph_db) {
        _write_dataset(target, graph_db);
    }
    void write_dataset(const fs::path &target, const ::thesis::mdl_db_t &db) {
        _write_dataset(target, db);
    }

}  // namespace tud
