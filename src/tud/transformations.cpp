#include <helper/flat_map.hpp>
#include "helper/db_access.hpp"
#include "helper/misc.hpp"
#include "tud/transformations.hpp"
#include "tud/graph_db.hpp"
#include "tud/edge_data.hpp"
#include "tud/temporal_node.hpp"
#include <map>
#include <konect/transformations.hpp>

namespace tud {
    using namespace helper;
    auto replace_temporal_nodes(input_db_t::graph_t &&in, time_t incubation_period) -> transformed_db_t::graph_t {
        transformed_db_t::graph_t ret;
        ret.reserve(in.num_nodes());

        for (auto node: in.nodes()) {
            [[maybe_unused]] auto id = ret.add_node(node->label().by_index(0).m_label);
            assert(id == node.get_id());
        }

        for (auto edge: in.edges()) {
            ret.add_edge(edge->from().get_id(), edge->to().get_id(), edge->label());
        }

        for (const auto node: in.nodes()) {
            if (!node->label().changes()) {
                continue;
            }

            auto ret_node = ret[node.get_id()];

            flat_map_t<time_t, graph::node_id_t> time_to_id;
            flat_set(time_to_id, node->label().by_index(0).m_time, node.get_id());
            auto last_id = node.get_id();
            for (const auto change: node->label().all()) {
                if (change.m_time == node->label().by_index(0).m_time) {
                    // The first label is spam, this node is infected from the start
                    ret_node->label() = change.m_label;
                } else {
                    const auto new_id = ret.add_node(change.m_label);
                    flat_set(time_to_id, change.m_time, new_id);
                    ret.add_edge(last_id, new_id, change.m_time - incubation_period, dist_t{0});
                    last_id = new_id;
                }
            }

            const auto find_node = [&](time_t time) {
                const auto last_change = node->label().by_time(time).m_time;
                const auto it = flat_find(time_to_id, last_change);
                assert(it != end(time_to_id));
                return it->second;
            };

            for (const auto dir: {graph::e_Outgoing, graph::e_Incoming}) {
                for (const auto edge: ret_node->incident_edges(dir)) {
                    const auto id = find_node(edge->label().time);
                    ret.move_edge(edge.get_id(), id, dir);
                }
            }
        }

        return ret;
    }

    auto replace_temporal_nodes(input_db_t &&input, time_t incubation_period) -> transformed_db_t {
        transformed_db_t ret;
        for (auto index = 0ul; index < input.size(); ++index) {
            ret.add_graph(input.label(index), replace_temporal_nodes(std::move(input[index]), incubation_period));
        }

        return ret;
    }
    void globally_minimize_times(transformed_db_t &input) {
        std::map<time_t, time_t> time_map;
        map_all_edges(input, [&](auto edge_handle) { time_map.emplace(edge_handle->label().time, time_t{}); });

        time_t cur_time{};
        for (auto &pair: time_map) {
            pair.second = cur_time;
            preinc(cur_time);
        }

        map_all_edges(input,
                      [&](auto edge_handle) { edge_handle->label().time = time_map[edge_handle->label().time]; });
    }
    void shift_times_to_null(transformed_db_t &input) {
        time_t min_time{std::numeric_limits<time_t::UnderlyingType>::max()};
        map_all_edges(input, [&](auto edge_handle) { update_min(min_time, edge_handle->label().time); });
        map_all_edges(input, [&](auto edge_handle) { edge_handle->label().time -= min_time; });
    }
    auto full_transform(input_db_t &&input, time_t incubation_period) -> transformed_db_t {
        auto ret = replace_temporal_nodes(std::move(input), incubation_period);
        globally_minimize_times(ret);
        return ret;
    }
}  // namespace tud
