#include "tud/read_data.hpp"
#include "tud/graph_db.hpp"
#include "tud/temporal_node.hpp"
#include "tud/edge_data.hpp"
#include <fstream>
#include <tuple>
#include <vector>
#include "helper/io.hpp"

namespace tud {
    using namespace helper;

    auto read_edges(const fs::path &adjacency, const fs::path &attributes, input_db_t &graph_db,
                    const std::vector<global_node_index_t> &node_indicator) {
        auto add_edge = [&graph_db,
                         &node_indicator](size_t from, char /* comma separator */, size_t to, int attribute) {
            // Sadly, the input is 1-based.
            --from;
            --to;
            auto graph_index = node_indicator[from].graph_index;
            graph_db[graph_index].add_edge(
                node_indicator[from].member_index, node_indicator[to].member_index, tud::time_t{attribute});
        };
        if (exists(attributes)) {
            helper::read_parallel<std::tuple<size_t, char, size_t>, int>(add_edge, adjacency, attributes);
        } else {
            helper::read_to<size_t, char, size_t>(
                adjacency, [add_edge](size_t from, char comma, size_t to) { add_edge(from, comma, to, 0); });
        }
    }

    auto read_node_labels(const fs::path &node_labels, input_db_t &graph_db,
                          const std::vector<global_node_index_t> &node_indicator) {
        using read_t = input_db_t ::node_label_t::label_change_t;
        auto read_node_label = [&graph_db, &node_indicator, index = 0ul](std::vector<read_t> &&line) mutable {
            const auto indicator = node_indicator[index++];
            graph_db[indicator.graph_index][indicator.member_index]->label().set_labels(std::move(line));
        };

        helper::read_line_to<read_t>(node_labels, read_node_label);
    }

    auto read_dataset(const fs::path &directory) -> input_db_t {
        auto file_name_gen = filename_generator(directory);
        const auto adjacency_file = file_name_gen(adjacency_appendix);
        const auto graph_indicator_file = file_name_gen(graph_indicator_appendix);
        const auto graph_label_file = file_name_gen(graph_label_appendix);
        const auto edge_attributes_file = file_name_gen(edge_attributes_appendix);
        const auto node_labels_file = file_name_gen(node_labels_appendix);

        input_db_t ret;
        helper::read_to<int>(graph_label_file, [&ret](int label) { ret.add_graph(label); });

        std::vector<global_node_index_t> node_indicator;
        helper::read_to<size_t>(graph_indicator_file, [&node_indicator, &ret](size_t index) {
            --index;
            auto node_id = ret[index].add_node();
            node_indicator.push_back(global_node_index_t{index, node_id});
        });

        read_node_labels(node_labels_file, ret, node_indicator);

        read_edges(adjacency_file, edge_attributes_file, ret, node_indicator);
        return ret;
    }
}  // namespace tud
