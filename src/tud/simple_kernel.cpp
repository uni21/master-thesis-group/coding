#include <numeric>
#include "tud/graph_db.hpp"
#include "tud/temporal_node.hpp"
#include "tud/simple_kernel.hpp"
#include "tud/statistics.hpp"

namespace tud {
    auto histogram_feature(const input_db_t::graph_t &graph, size_t partitions, bool cumulative) {
        feature_t ret(partitions + 1);
        for (const auto node: graph.nodes()) {
            if (!node->label().changes()) {
                continue;
            }
            const auto infections = get_infections_of_node(node);
            if (infections.encounters == 0) {
                ++ret[0];
            } else {
                const auto [infec, enc] = infections;
                const auto index = (infec * partitions + enc - 1) / enc;
                assert(index <= partitions);
                if (cumulative) {
                    for (auto i = 0ul; i < index; ++i) {
                        ++ret[i];
                    }
                }

                ++ret[index];
            }
        }

        return ret;
    }

    auto infectiousness_histogram(const input_db_t &db, size_t partitions, bool cumulative) -> std::vector<feature_t> {
        std::vector<feature_t> ret;
        ret.reserve(db.size());

        for (const auto &g: db) {
            ret.push_back(histogram_feature(g, partitions, cumulative));
        }

        return ret;
    }
    auto calculate_gram(const std::vector<feature_t> &feature_map) -> gram_t {
        const auto dim = feature_map.size();
        gram_t ret(dim);

        for (auto row = 0ul; row < dim; ++row) {
            for (auto col = 0ul; col < dim; ++col) {
                ret[row].push_back(
                    std::inner_product(begin(feature_map[row]), end(feature_map[row]), begin(feature_map[col]), 0ul));
            }
        }
        return ret;
    }
}  // namespace tud