#include <functionalities.hpp>
#include <minimize_times.hpp>
#include <mdl.hpp>
#include "konect/graph_io.hpp"
#include "konect/transformations.hpp"
#include "konect/graph.hpp"
#include "tud/all_defs.hpp"
#include "tud/read_data.hpp"
#include "tud/transformations.hpp"
#include "tud/simple_kernel.hpp"
#include "tud/write_data.hpp"
#include "graph_statistics.hpp"

namespace thesis {
    using namespace helper;
    auto read_konect_to_transformed(const ProgArgs &args) -> tud::transformed_db_t {
        auto graph = konect::read_graph(args.m_path);
        return konect::to_db(std::move(graph));
    }

    auto read_tud(const ProgArgs &args) -> tud::transformed_db_t {
        return tud::replace_temporal_nodes(read_to_tud_input(args), args.m_incubation_period);
    }

    auto read_to_transformed(const ProgArgs &args) -> tud::transformed_db_t {
        switch (args.m_input_type) {
            case InputType::TUD: return read_tud(args);
            case InputType::Konect: return read_konect_to_transformed(args);
            case InputType::Detect: std::cout << "Could not detect input type.\n"; exit(1);
        }
        exit(1);
    }

    auto read_to_tud_input(const ProgArgs &args) -> tud::input_db_t {
        if (args.m_input_type != InputType::TUD) {
            std::cout << "Can only do this with TUD datasets.\n";
            exit(1);
        }
        auto ret = tud::read_dataset(args.m_path / args.dataset_name);
        const auto stats = get_statistics(ret);
        std::cout << stats << "\n\n";
        return ret;
    }

    void dump_simple_kernel(const ProgArgs &args, const tud::Kernel &kernel) {
        auto [outpath, specific_outfile] = get_output_path(args);

        if (!specific_outfile) {
            outpath += "__SimpleKernel_";
            if (args.m_simple_cumulative) {
                outpath += "cumulative_";
            }
            outpath += std::to_string(args.m_simple_partitions);
            outpath += ".fmap";
            auto feature_out = open_write(outpath);
            tud::write_feature_maps(kernel.m_fmaps, feature_out);
            std::cout << "Wrote feature maps to " << absolute(outpath) << '\n';
        }

        if (!specific_outfile) {
            outpath.replace_extension("gram");
        }
        auto gram_out = open_write(outpath);
        tud::write_gram(kernel.m_gram, kernel.m_db, gram_out);
        std::cout << "Wrote gram matrix to " << absolute(outpath) << '\n';
    }

    template<class DB>
    void dump_db(const ProgArgs &args, const DB &db, const std::string_view name, const std::string &suffix = "",
                 bool force_suffix = false) {
        auto [outpath, specific_outfile] = get_output_path(args, suffix, force_suffix);
        if (specific_outfile) {
            std::cout << "Can only save the transformed graph to a directory.\n";
            exit(1);
        }

        tud::write_dataset(outpath, db);
        std::cout << "Wrote " << name << " graph to " << absolute(outpath.parent_path()) << '\n';
    }

    void dump_transformed(const ProgArgs &args, const tud::transformed_db_t &db) {
        const auto suffix = args.m_transform_only ? "_trans" : "_trans_min";
        const auto name = args.m_transform_only ? "transformed" : "minimized";
        dump_db(args, db, name, suffix);
    }

    void dump_mdl(const ProgArgs &args, const mdl_db_t &db) {
        dump_db(args, db, "mdl", "_MDL", true);
    }

    auto calc_simple_kernel(const ProgArgs &args, const tud::input_db_t &db) -> tud::Kernel {
        auto histogram = tud::infectiousness_histogram(db, args.m_simple_partitions, args.m_simple_cumulative);
        return {histogram, tud::calculate_gram(histogram), db};
    }
    void calculate_minimal_times(const ProgArgs &args, tud::transformed_db_t &db, helper::Stopwatch &watch) {
        if (args.m_incubation_period == 0_t && args.m_recovery_period == eternity && !args.m_dump_mdl &&
            !args.m_use_mdl) {
            std::cout << "Using fast time minimization specialization.\n";
            watch.lap();
            compute_minimal_times(db);
            watch.print_lap("Minimization took ");
        } else {
            std::cout << "Using general minimization.\n";
            auto mdl = construct_mdl(db, args.m_incubation_period, args.m_recovery_period);
            watch.print_lap("Constructing mdl took ");
            longest_paths(mdl);
            watch.print_lap("Computing longest paths in mdl took ");
            if (args.m_dump_mdl) {
                dump_mdl(args, mdl);
                watch.print_lap("Dumping MDL took ");
            }
            map_back_times(mdl, db);
            watch.print_lap("Remapping times took ");
        }
    }

}  // namespace thesis
