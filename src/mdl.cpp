#include "mdl.hpp"
#include "tud/all_defs.hpp"
#include <graph/store.hpp>
#include <helper/misc.hpp>

namespace thesis {
    using namespace graph::literals;
    using mdl_t = mdl_db_t::graph_t;

    auto convert(graph::edge_id_t id) -> graph::node_id_t {
        return graph::node_id_t{id.get()};
    }
    auto construct_mdl(const tud::transformed_db_t::graph_t &input, time_t incubation_period, time_t recovery_period)
        -> mdl_t {
        mdl_t ret;

        ret.reserve(convert(input.num_edges()));
        helper::repeat_n(input.num_edges(), [&]() { ret.add_node(1); });

        for (auto edge: input.edges()) {
            const auto to = edge->to();
            const auto from_edge_node = convert(edge.get_id());
            assert(from_edge_node < ret.num_nodes());
            const auto time = edge->label().time;
            for (auto outgoing: to->outgoing_edges()) {
                const auto diff = outgoing->label().time - time;
                const auto to_edge_node = convert(outgoing.get_id());
                assert(to_edge_node < ret.num_nodes());
                if (diff <= incubation_period) {
                    ret.add_edge(to_edge_node, from_edge_node, -incubation_period.get());
                    continue;
                }
                if (diff > recovery_period) {
                    ret.add_edge(from_edge_node, to_edge_node, recovery_period.get() + 1);
                    continue;
                }

                ret.add_edge(from_edge_node, to_edge_node, incubation_period.get() + 1);
                if (recovery_period != thesis::eternity) {
                    // Seems a bit dangerous for Moore-Bellman-Ford to have a big edge in here.
                    ret.add_edge(to_edge_node, from_edge_node, -recovery_period.get());
                }
            }
        }

        return ret;
    }

    auto construct_mdl(const tud::transformed_db_t &db, time_t incubation_period, time_t recovery_period) -> mdl_db_t {
        mdl_db_t ret;
        for (const auto &graph: db) {
            ret.add_graph(0, construct_mdl(graph, incubation_period, recovery_period));
        }

        return ret;
    }

    bool check_potential(mdl_t &graph) {
        for (auto edge: graph.edges()) {
            if (edge->to()->label() < edge->from()->label() + edge->label()) {
                return false;
            }
        }
        return true;
    }

    void longest_paths(mdl_t &graph) {
        auto relax = [&](const mdl_t::edge_handle_t &edge) {
            helper::update_max(edge->to()->label(), edge->from()->label() + edge->label());
        };

        helper::repeat_n(graph.num_nodes() - 1_node, [&]() {
            for (const auto &edge: graph.edges()) {
                relax(edge);
            }
        });

        assert(check_potential(graph));
    }

    void longest_paths(mdl_db_t &db) {
        for (auto &g: db) {
            longest_paths(g);
        }
    }
    void map_back_times(mdl_db_t &mdl, tud::transformed_db_t &transformed) {
        for (auto i = 0ul; i < transformed.size(); ++i) {
            for (auto edge: transformed[i].edges()) {
                edge->label().time = time_t{mdl[i][convert(edge.get_id())]->label()};
            }
        }
    }
}  // namespace thesis
