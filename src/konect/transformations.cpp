#include "konect/transformations.hpp"

#include "tud/transformations.hpp"
#include "tud/graph_db.hpp"
#include "helper/misc.hpp"
#include "tud/edge_data.hpp"
namespace konect {
    auto to_tud_type(Graph::graph_t &&graph) -> tud::transformed_db_t::graph_t {
        tud::transformed_db_t::graph_t ret;
        ret.reserve(graph.num_nodes());
        ret.reserve(graph.num_edges());

        helper::repeat_n(graph.num_nodes(), [&]() { ret.add_node(); });

        for (const auto eh: graph.edges()) {
            assert(eh->from().get_id() < ret.num_nodes());
            assert(eh->to().get_id() < ret.num_nodes());
            ret.add_edge(eh->from().get_id(), eh->to().get_id(), tud::time_t{eh->label().time.ns.count()});
        }
        return ret;
    }
    auto to_db(Graph &&graph) -> tud::transformed_db_t {
        tud::transformed_db_t ret;
        ret.add_graph(0, to_tud_type(std::move(graph.m_graph)));

        tud::globally_minimize_times(ret);
        return ret;
    }
}  // namespace konect