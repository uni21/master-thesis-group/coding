#include <helper/misc.hpp>
#include "konect/common.hpp"
#include "konect/graph_io.hpp"
#include "tud/edge_data.hpp"

namespace konect {
    using namespace graph::literals;

    bool maybe_ignore_line(std::ifstream &istream) {
        istream >> std::ws;

        while (istream.peek() == '%') {
            istream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        return istream.good();
    }

    [[nodiscard]] auto extract_types(std::ifstream &istream) -> std::tuple<NetworkType, WeightType> {
        assert(istream.peek() == '%');
        istream.get();

        std::string network_type_name, weight_type_name;
        istream >> network_type_name >> weight_type_name >> std::ws;
        assert(istream);

        auto network_type_it = std::find(begin(NetworkTypeKey), end(NetworkTypeKey), network_type_name);
        auto weight_type_it = std::find(begin(WeightTypeKey), end(WeightTypeKey), weight_type_name);
        assert(network_type_it != end(NetworkTypeKey) && weight_type_it != end(WeightTypeKey));

        return {static_cast<NetworkType>(network_type_it - begin(NetworkTypeKey)),
                static_cast<WeightType>(weight_type_it - begin(WeightTypeKey))};
    }

    bool maybe_reserve(std::ifstream &istream, Graph &ret) {
        if (istream.peek() != '%') {
            return false;
        }
        istream.get();

        ret.m_meta_data.emplace();
        [[maybe_unused]] const auto success = helper::read_tuple(istream, *ret.m_meta_data);
        assert(success);
        istream >> std::ws;

        const auto num_edges = std::get<0>(*ret.m_meta_data);
        ret.m_graph.reserve(num_edges + num_edges);

        const auto num_nodes = ret.effective_num_nodes();
        ret.m_graph.reserve(num_nodes);
        helper::repeat_n(num_nodes, [&]() { ret.m_graph.add_node(); });

        return true;
    }

    auto generate_filename(const fs::path &dir, const std::string &file_prefix = "out") {
        // Find the directory name
        assert(fs::is_directory(dir));
        auto directory_name = dir.filename();
        if (directory_name.empty()) {
            directory_name = dir.parent_path().filename();
        }
        return dir / (file_prefix + "." + directory_name.native());
    }

    auto read_graph(const fs::path &dir) -> Graph {
        assert(exists(dir));
        Graph ret;
        const auto file = generate_filename(dir);
        assert(exists(file));
        std::ifstream istream{file};

        assert(istream);
        std::tie(ret.m_network_type, ret.m_weight_type) = extract_types(istream);

        maybe_reserve(istream, ret);

        while (maybe_ignore_line(istream)) {
            std::tuple<graph::node_id_t, graph::node_id_t, weight_t, time_t> tup;
            [[maybe_unused]] const auto success = helper::read_tuple(istream, tup);
            assert(success);

            auto [from, to, weight, time] = tup;
            if (from == to || !istream) {
                // Some of these "bipartite graphs" have loops
                continue;
            }

            const auto max_node = std::max(from, to);
            while (max_node >= ret.m_graph.num_nodes()) {
                ret.m_graph.add_node();
            }
            ret.m_graph.add_edge(from, to, EdgeLabel{weight, time});
        }

        return ret;
    }

    auto copy_meta_data(const fs::path &target, const fs::path &source) {
        if (is_empty(source)) {
            return;
        }

        for (const auto pref: {"meta", "README"}) {
            const auto src_file = generate_filename(source, pref);
            if (exists(src_file)) {
                copy(src_file, generate_filename(target, pref));
            }
        }
    }

    void write_types(std::ofstream &ostream, const Graph &graph) {
        ostream << "% " << NetworkTypeKey[static_cast<size_t>(graph.m_network_type)] << '\t'
                << WeightTypeKey[static_cast<size_t>(graph.m_weight_type)] << '\n';
    }

    void maybe_write_meta_data(std::ofstream &ostream, const Graph &graph) {
        const auto meta_data = graph.get_meta_data();
        if (!meta_data) {
            return;
        }

        ostream << "% ";
        helper::write_tuple(ostream, *meta_data, "\t");
        ostream << '\n';
    }

    void write_graph(const fs::path &dir, const Graph &graph, const fs::path &src_dir) {
        create_directories(dir);
        copy_meta_data(dir, src_dir);

        const auto out_file = generate_filename(dir);
        std::ofstream ostream{out_file};

        write_types(ostream, graph);
        maybe_write_meta_data(ostream, graph);

        for (const auto edge: graph.m_graph.edges()) {
            const auto [weight, time] = edge->label();
            helper::write_delimited_vars(ostream, "\t", edge->from().get_id(), edge->to().get_id(), weight, time);
            ostream << '\n';
        }
    }
}  // namespace konect
