#include "minimize_times.hpp"
#include "graph/nanorange.hpp"
#include "graph/store.hpp"
#include "helper/misc.hpp"
#include "tud/edge_data.hpp"
#include <NamedType/named_type.hpp>

namespace thesis {
    using mark_t = size_t;
    using graph_t = tud::transformed_db_t::graph_t;
    using edge_id_t = graph::edge_id_t;
    static constexpr auto min_time = tud::time_t{1};
    using namespace graph::literals;
    template<class T>
    using store_t = graph::edge_store_t<T, graph_t>;

    using dfs_index = fluent::NamedType<size_t, struct _dfs_index, fluent::Arithmetic>;
    static constexpr auto unvisited = dfs_index{helper::numeric_limits<dfs_index>::max()};

    auto time_diff(graph::EdgeDirection dir) -> tud::time_t {
        if (dir == graph::e_Incoming) {
            return tud::time_t{1};
        }
        return {};
    }

    template<class Fn>
    void for_edges_after(const graph_t::const_edge_handle_t handle, Fn &&f) {
        const auto time = handle->label().time;
        auto outgoing_edges = handle->to()->outgoing_edges() |
                              nano::views::filter([time](auto outgoing) { return outgoing->label().time <= time; });
        nano::for_each(outgoing_edges,
                       [&](auto &&arg) { return f(std::forward<decltype(arg)>(arg), graph::e_Outgoing); });
        auto incoming_edges = handle->from()->incoming_edges() |
                              nano::views::filter([time](auto incoming) { return incoming->label().time < time; });
        nano::for_each(incoming_edges,
                       [&](auto &&arg) { return f(std::forward<decltype(arg)>(arg), graph::e_Incoming); });
    }

    struct tarjan_state {
        explicit tarjan_state(graph_t &g) :
                g(g), indices(g, unvisited), earliest(g, unvisited), new_times(g, min_time), on_stack(g, false) {
            component_stack.reserve(g.num_edges().get());
        }

        void visit_first(edge_id_t to_visit, std::vector<edge_id_t> &frontier) {
            assert(indices[to_visit] == unvisited);
            indices[to_visit] = current_index;
            earliest[to_visit] = current_index;
            helper::preinc(current_index);
            component_stack.push_back(to_visit);
            on_stack[to_visit] = true;

            for_edges_after(g[to_visit], [&](const auto edge_handle, auto direction) {
                if (handle_connection(to_visit, edge_handle, direction)) {
                    frontier.push_back(edge_handle.get_id());
                }
            });
        }

        bool handle_connection(edge_id_t from, graph_t::const_edge_handle_t to, graph::EdgeDirection direction) {
            if (indices[to] != unvisited) {
                if (on_stack[to]) {
                    helper::update_min(earliest[from], indices[to]);
                } else {
                    // to is already permanent and has a right time
                    helper::update_max(new_times[from], new_times[to] + time_diff(direction));
                }
                return false;
            } else {
                return true;
            }
        }

        [[nodiscard]] auto collect_max_times(time_t max_time,
                                             const std::vector<edge_id_t>::const_reverse_iterator &rend) const
            -> time_t {
            // Collect the maximum of all times of neighbors of the current component.
            std::for_each(component_stack.crbegin(), rend, [&](auto id) {
                for_edges_after(g[id], [&](auto handle, auto direction) {
                    assert(indices[handle] != unvisited);
                    if (!on_stack[handle]) {
                        helper::update_max(max_time, new_times[handle] + time_diff(direction));
                    }
                });
                helper::update_max(max_time, new_times[id]);
            });
            return max_time;
        }

        auto current_component_end(edge_id_t current_edge) {
            return ++std::find(component_stack.rbegin(), component_stack.rend(), current_edge);
        }

        auto finalize_component(edge_id_t current) -> time_t {
            const auto rend = current_component_end(current);
            const auto max_time = collect_max_times(new_times[current], rend);

            std::for_each(component_stack.rbegin(), rend, [max_time, this](auto id) {
                new_times[id] = max_time;
                on_stack[id] = false;
            });

            const auto forward = rend.base();
            assert(*forward == current);
            component_stack.erase(forward, std::end(component_stack));
            return max_time;
        }

        tarjan_state(const tarjan_state &) = delete;
        tarjan_state &operator=(const tarjan_state &) = delete;

        graph_t &g;
        store_t<dfs_index> indices;
        store_t<dfs_index> earliest;
        store_t<tud::time_t> new_times;
        store_t<mark_t> on_stack;
        std::vector<edge_id_t> component_stack;
        dfs_index current_index{0};
    };

    auto tarjan(edge_id_t start, tarjan_state &state, tud::time_t max_global_time) -> tud::time_t {
        std::vector<edge_id_t> frontier{start};

        while (!frontier.empty()) {
            const auto current = frontier.back();
            const auto index = state.indices[current];

            if (index == unvisited) {
                state.visit_first(current, frontier);
            } else {
                frontier.pop_back();

                if (state.on_stack[current]) {
                    for_edges_after(state.g[current], [&](auto handle, auto /*direction*/) {
                        if (state.on_stack[handle]) {
                            helper::update_min(state.earliest[current], state.earliest[handle]);
                        }
                    });
                    if (state.indices[current] == state.earliest[current]) {
                        // This means, the current edge is the last member of the current component in the frontier, so
                        // we can finalize it.
                        helper::update_max(max_global_time, state.finalize_component(current));
                    }
                }
            }
        }
        return max_global_time;
    }

    void compute_minimal_times(graph_t &graph) {
        tarjan_state state{graph};
        tud::time_t max_time = min_time;

        for (const auto edge: graph.edges()) {
            if (state.indices[edge] == unvisited) {
                max_time = tarjan(edge.get_id(), state, max_time);
                assert(state.component_stack.empty());
            }
        }

        nano::for_each(graph.edges(), [&](auto edge) { edge->label().time = state.new_times[edge.get_id()]; });
    }

    void compute_minimal_times(tud::transformed_db_t &db) {
        for (auto &graph: db) {
            compute_minimal_times(graph);
        }
    }

}  // namespace thesis
