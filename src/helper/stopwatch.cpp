#include "helper/stopwatch.hpp"
#include <iostream>
using namespace std::chrono_literals;

namespace helper {
    template<class Division, class Duration>
    auto print_above(Duration d, Division div, std::string_view unit) {
        if (d > div) {
            const auto below = std::chrono::floor<Division>(d);
            d -= below;
            std::cout << below.count() << unit;
        }
        return d;
    }

    template<class Duration>
    auto print_duration(Duration d) {
        d = print_above(d, 1h, "h");
        d = print_above(d, 1min, "m");
        d = print_above(d, 1s, "s");
        d = print_above(d, 1ms, "ms");
        std::cout << '\n';
    }

    Stopwatch::Stopwatch() : m_start(clock_t::now()), m_last(m_start) {}

    auto Stopwatch::print_lap(std::string_view text) -> duration_t {
        const auto duration = clock_t::now() - m_last;
        if (text.empty()) {
            std::cout << "Last interval took ";
        } else {
            std::cout << text;
        }

        print_duration(duration);

        m_last = clock_t::now();
        return duration;
    }
    auto Stopwatch::lap() -> duration_t {
        const auto duration = clock_t::now() - m_last;
        m_last = clock_t::now();
        return duration;
    }
    auto Stopwatch::print_total(std::string_view text) const -> duration_t {
        const auto duration = clock_t::now() - m_start;
        if (text.empty()) {
            std::cout << "In total took ";
        } else {
            std::cout << text;
        }
        print_duration(duration);
        return duration;
    }
    auto Stopwatch::total() const -> duration_t {
        return clock_t::now() - m_start;
    }
}  // namespace helper
