#include <helper/argument_parser.hpp>

namespace helper {
    auto extract_dataset_name(const fs::path &directory) -> std::string {
        if (!is_directory(directory)) {
            throw QuickArgParserInternals::ArgumentError(fs::absolute(directory).string() + " is not a directory.");
        }
        auto abs = fs::absolute(directory);
        auto non_empty = abs.end();
        for (auto it = abs.begin(); it != abs.end(); ++it) {
            if (!it->empty()) {
                non_empty = it;
            }
        }
        if (non_empty == abs.end()) {
            throw QuickArgParserInternals::ArgumentError("Cannot determine a dataset name from " + directory.string());
        }

        return non_empty->string();
    }
    auto get_parsed(int argc, char **argv) -> ProgArgs {
        try {
            auto parsed = ProgArgs{{argc, argv}};
            parsed.dataset_name = extract_dataset_name(parsed.m_path);
            if (parsed.m_input_type == thesis::InputType::Detect) {
                parsed.m_input_type = helper::guess_format(parsed.m_path, parsed.dataset_name);
            }

            return parsed;
        } catch (const QuickArgParserInternals::ArgumentError &e) {
            std::cout << "Error when trying to parse arguments:\n" << e.what();
            std::cout << "\nCall " << argv[0] << " --help for more information.\n";
            exit(1);
        }
    }
}  // namespace helper