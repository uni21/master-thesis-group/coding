#include "helper/io.hpp"
#include <cassert>

namespace helper {
    template<class Duration>
    auto get_time(std::istream &inp) -> Duration {
        typename Duration::rep tmp;
        inp >> tmp;
        return Duration{tmp};
    }

    auto read_unix_time(std::istream &inp) -> std::chrono::nanoseconds {
        using namespace std::chrono;
        // This can be either an int or a floating point number that are actually two ints (seconds and nanoseconds)
        // We first obtain the the seconds as integer and then the fractional part if it exists as double and convert
        // everything to nanoseconds.
        // Just reading this always as double is NOT exact, since the nanoseconds need up to 30 bits and the seconds
        // are currently also at about 30 bits. Even the typical (not guaranteed!) mantissa of long double is only 64,
        // which is not really future proof.

        if (!inp) {
            return {};
        }
        auto secs = get_time<seconds>(inp);
        assert(inp);

        if (inp.peek() == '.') {
            // Read as seconds, absolutely no problem regarding exactness since a billion is far below the mantissa
            auto nano_as_secs = get_time<duration<double>>(inp);
            assert(inp);

            return secs + duration_cast<nanoseconds>(nano_as_secs);
        }

        return duration_cast<nanoseconds>(secs);
    }

    auto open_write(const fs::path &target) -> std::ofstream {
        std::ofstream out{target};
        if (!out) {
            std::cout << "Cannot write to " << target << '\n';
            exit(1);
        }
        return out;
    }
    auto get_output_path(const ProgArgs &args, std::string_view suffix, bool force_suffix)
        -> std::pair<fs::path, bool> {
        auto outpath = args.m_output.empty() ? args.m_path : args.m_output;
        if (args.m_output.empty() || force_suffix) {
            outpath += suffix;
        }
        if (!exists(outpath)) {
            create_directories(outpath);
        }

        const bool specific_outfile = !is_directory(outpath);

        if (!specific_outfile) {
            outpath /= outpath.stem();
        }

        return {outpath, specific_outfile};
    }
}  // namespace helper