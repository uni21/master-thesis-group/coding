#include <helper/filesystem.hpp>

namespace helper {
    auto guess_format(const fs::path &directory, std::string_view dataset_name) -> thesis::InputType {
        auto adjacency_file = directory / dataset_name;
        adjacency_file += "_A.txt";
        if (fs::exists(adjacency_file)) {
            return thesis::InputType::TUD;
        }
        return thesis::InputType::Konect;
    }
}  // namespace helper
