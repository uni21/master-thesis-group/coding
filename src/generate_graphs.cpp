#include "generate_graphs.hpp"
#include "tud/graph_db.hpp"
#include "tud/temporal_node.hpp"
#include "tud/edge_data.hpp"
#include "helper/misc.hpp"

namespace thesis {
    using namespace tud;
    using namespace helper;
    using graph_t = input_db_t::graph_t;

    auto populate_static_graph(graph_t &graph, double avg_degree) {
        const auto time_gen = [](tud::time_t cur_time) { return cur_time == tud::time_t{}; };
        auto degree_gen = [mt = get_mersenne_twister(), degree_dist = std::poisson_distribution<size_t>(avg_degree)](
                              auto &&...) mutable -> size_t { return degree_dist(mt); };

        auto neighbor_gen = [mt = get_mersenne_twister(),
                             neighbor_dist = std::uniform_int_distribution<size_t>{0, graph.num_nodes().get() - 2}](
                                graph_t &graph,
                                auto && /* number of neighbor */,
                                const graph_t::node_handle_t from,
                                auto &&...) mutable {
            auto to = graph::node_id_t{neighbor_dist(mt)};
            // We do not want any loops.
            to += graph::node_id_t{from.get_id() <= to};
            graph.template add_edge(from.get_id(), to);
        };

        generate_graph(graph, time_gen, degree_gen, neighbor_gen);
    }

    auto generate_static_graph(size_t num_nodes, size_t avg_degree) -> graph_t {
        graph_t ret;

        ret.reserve(graph::node_id_t{num_nodes});
        helper::repeat_n(num_nodes, [&]() { ret.add_node(); });

        // Halving since the both in and outgoing edges come from this.
        populate_static_graph(ret, static_cast<double>(avg_degree) / 2);

        return ret;
    }

    auto random_static_graph_db(size_t num_graphs, size_t num_nodes, size_t avg_degree) -> input_db_t {
        input_db_t ret;
        ret.reserve(num_graphs);

        helper::repeat_n(num_graphs, [&]() { ret.add_graph(0, generate_static_graph(num_nodes, avg_degree)); });

        return ret;
    }
}  // namespace thesis
