#include "checks/time_equivalent.hpp"
#include "tud/temporal_node.hpp"
#include "tud/edge_data.hpp"
#include <cassert>

namespace thesis::checks {
    using graph_t = tud::input_db_t::graph_t;

    bool check_edge(graph_t::const_edge_handle_t to_check, const graph_t &other) {
        const auto this_time = to_check->label().time;
        const auto other_time = other[to_check.get_id()]->label().time;

        return nano::all_of(to_check->to()->outgoing_edges(), [&](const auto outgoing) {
            const auto out_time = outgoing->label().time;
            const auto other_out_time = other[outgoing.get_id()]->label().time;
            if (this_time < out_time) {
                return other_time < other_out_time;
            }
            return other_time >= other_out_time;
        });
    }

    bool check_node(graph_t::const_node_handle_t to_check, const graph_t &other) {
        const auto &other_label = other[to_check.get_id()]->label();

        return nano::all_of(to_check->incoming_edges(), [&](const auto incoming) {
            const auto index_this = to_check->label().first_index_after(incoming->label().time);
            const auto index_other = other_label.first_index_after(other[incoming.get_id()]->label().time);
            return index_other <= index_this;
        });
    }

    bool check_time_equivalent(const graph_t &original, const graph_t &minimized) {
        // The graphs should already be known to be nearly equivalent, passing two graphs with differing edge numbers is
        // a bug
        assert(original.num_edges() == minimized.num_edges());

        return nano::all_of(original.edges(), [&](const auto edge) { return check_edge(edge, minimized); }) &&
               nano::all_of(original.nodes(), [&](const auto node) { return check_node(node, minimized); });
    }

    auto get_time_diff(const graph_t &original, const graph_t &minimized, tud::time_t original_min) -> tud::time_t {
        tud::time_t sum{};
        nano::for_each(original.edges(), [&](const auto edge_handle) {
            const auto id = edge_handle.get_id();
            const auto diff = (edge_handle->label().time - original_min) - minimized[id]->label().time;
            assert(diff >= tud::time_t{});
            sum += diff;
        });
        return sum;
    }
}  // namespace thesis::checks
