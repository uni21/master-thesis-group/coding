#!/bin/sh

DATASETS="""dblp_ct1.zip
dblp_ct2.zip
facebook_ct1.zip
facebook_ct2.zip
highschool_ct1.zip
highschool_ct2.zip
infectious_ct1.zip
infectious_ct2.zip
mit_ct1.zip
mit_ct2.zip
tumblr_ct1.zip
tumblr_ct2.zip"""

BASEURL="https://www.chrsmrrs.com/graphkerneldatasets/"

mkdir -p data
cd data

for i in $DATASETS; do
  wget $BASEURL$i
  unzip $i
done
