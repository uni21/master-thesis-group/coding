#ifndef THESIS_MINIMIZE_TIMES_HPP_04F12678BBB74C5CBDC11AE909438DFD
#define THESIS_MINIMIZE_TIMES_HPP_04F12678BBB74C5CBDC11AE909438DFD

#include "tud/common.hpp"
#include "tud/graph_db.hpp"
#include "tud/temporal_node.hpp"

namespace thesis {
    void compute_minimal_times(tud::transformed_db_t::graph_t &input);
    void compute_minimal_times(tud::transformed_db_t &db);
}  // namespace thesis

#endif  // THESIS_MINIMIZE_TIMES_HPP_04F12678BBB74C5CBDC11AE909438DFD
