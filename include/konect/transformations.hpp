#ifndef THESIS_TRANSFORMATIONS_HPP_B13F302285E44540A024A6BF9104D854
#define THESIS_TRANSFORMATIONS_HPP_B13F302285E44540A024A6BF9104D854

#include "tud/common.hpp"
#include "konect/common.hpp"
#include "konect/graph.hpp"

namespace konect {
    auto to_db(Graph &&graph) -> tud::transformed_db_t;
}
#endif  // THESIS_TRANSFORMATIONS_HPP_B13F302285E44540A024A6BF9104D854
