#ifndef THESIS_GRAPH_IO_HPP_2B44BA1D6D86462A891318E39A374C4D
#define THESIS_GRAPH_IO_HPP_2B44BA1D6D86462A891318E39A374C4D

#include "konect/graph.hpp"
#include "helper/filesystem.hpp"

namespace konect {
    auto read_graph(const fs::path &dir) -> Graph;

    void write_graph(const fs::path &dir, const Graph &graph, const fs::path &src_dir = {});
}  // namespace konect

#endif  // THESIS_GRAPH_IO_HPP_2B44BA1D6D86462A891318E39A374C4D
