#ifndef THESIS_EDGE_LABEL_HPP_D234B5447F2C433E836E53FDD8F189FF
#define THESIS_EDGE_LABEL_HPP_D234B5447F2C433E836E53FDD8F189FF

#include "graph/graph.hpp"
#include "helper/io.hpp"
#include "common.hpp"

namespace konect {
    struct EdgeLabel {
        weight_t weight;
        time_t time;
    };
    // If this starts to fail we should change some things about EdgeLabel.
    static_assert(std::is_aggregate_v<EdgeLabel>);

    inline std::istream &operator>>(std::istream &istream, time_t &ns) {
        ns = time_t{helper::read_unix_time(istream)};
        return istream;
    }

    inline std::istream &operator>>(std::istream &istream, EdgeLabel &label) {
        istream >> label.weight >> label.time;
        return istream;
    }

    inline std::ostream &operator<<(std::ostream &ostream, const time_t &time) {
        const auto [sec, ns] = time.as_parts();
        ostream << sec.count();
        if (ns.count()) {
            ostream << std::setfill('0') << std::setw(9) << ns.count();
        }
        return ostream;
    }
}  // namespace konect

#endif  // THESIS_EDGE_LABEL_HPP_D234B5447F2C433E836E53FDD8F189FF
