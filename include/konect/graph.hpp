#ifndef THESIS_GRAPH_HPP_958267D2D9634A989F156D8BC31CEC13
#define THESIS_GRAPH_HPP_958267D2D9634A989F156D8BC31CEC13

#include <optional>
#include "graph/graph.hpp"
#include "common.hpp"
#include "edge_label.hpp"

namespace konect {

    struct Graph {
        using node_label_t = void;
        using edge_label_t = EdgeLabel;
        using meta_data_t = std::tuple<graph::edge_id_t, graph::node_id_t, graph::node_id_t>;
        using graph_t = graph::Graph<node_label_t, edge_label_t>;

        [[nodiscard]] auto is_bipartite() const -> bool {
            return m_network_type == NetworkType::Bipartite;
        }

        [[nodiscard]] auto is_undirected() const -> bool {
            return m_network_type != NetworkType::Directed;
        }

        auto generate_meta_data(bool override) -> bool {
            m_meta_data = get_meta_data(override);
            return m_meta_data.has_value();
        }

        [[nodiscard]] auto get_meta_data(bool override = false) const -> std::optional<meta_data_t> {
            if (m_meta_data && !override) {
                return m_meta_data;
            }
            if (is_bipartite()) {
                return {};
            }

            return meta_data_t{m_graph.num_edges(), m_graph.num_nodes(), m_graph.num_nodes()};
        }

        [[nodiscard]] auto effective_num_nodes() const -> graph::node_id_t {
            if (!m_meta_data) {
                return m_graph.num_nodes();
            }

            if (is_bipartite()) {
                return std::get<1>(*m_meta_data) + std::get<2>(*m_meta_data);
            }
            return std::get<1>(*m_meta_data);
        }

        NetworkType m_network_type;
        WeightType m_weight_type;
        graph_t m_graph;
        std::optional<meta_data_t> m_meta_data;
    };

    // We need to implement some constructors if this fails.
    static_assert(std::is_aggregate_v<Graph>);
}  // namespace konect

#endif  // THESIS_GRAPH_HPP_958267D2D9634A989F156D8BC31CEC13
