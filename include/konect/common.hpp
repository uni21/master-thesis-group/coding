#ifndef THESIS_COMMON_HPP_26DABDB426AB4660AA4B1B3565C0418C
#define THESIS_COMMON_HPP_26DABDB426AB4660AA4B1B3565C0418C

#include <chrono>
#include "graph/graph.hpp"

namespace konect {
    enum class NetworkType {
        Undirected,
        Directed,
        Bipartite,
    };

    constexpr static std::array<std::string_view, 3> NetworkTypeKey = {"sym", "asym", "bip"};

    enum class WeightType {
        Unweighted,
        Multi,
        Positive,
        Signed,
        MultiSigned,
        Rating,
        MultiRating,
        Dynamic,
    };

    constexpr static std::array<std::string_view, 8> WeightTypeKey = {
        "unweighted",     // WeightType::Unweighted
        "positive",       // WeightType::Multi
        "posweighted",    // WeightType::Positive
        "signed",         // WeightType::Signed
        "multisigned",    // WeightType::MultiSigned
        "weighted",       // WeightType::Rating
        "multiweighted",  // WeightType::MultiRating
        "dynamic",        // WeightType::Dynamic
    };

    struct Time {
        [[nodiscard]] auto as_parts() const -> std::pair<std::chrono::seconds, std::chrono::nanoseconds> {
            const auto sec_part = std::chrono::duration_cast<std::chrono::seconds>(ns);
            return {sec_part, ns - sec_part};
        }
        std::chrono::nanoseconds ns;
    };
    using time_t = Time;
    // The weights should be in a range such that using double for ints should still be accurate.
    using weight_t = double;

}  // namespace konect

#endif  // THESIS_COMMON_HPP_26DABDB426AB4660AA4B1B3565C0418C
