#ifndef THESIS_FILESYSTEM_HPP_97774A262F7D42D8B60B47FA7BFE3578
#define THESIS_FILESYSTEM_HPP_97774A262F7D42D8B60B47FA7BFE3578

#include <filesystem>
#include <string_view>
#include "common.hpp"

namespace fs = std::filesystem;

namespace helper {
    auto guess_format(const fs::path &directory, std::string_view dataset_name) -> thesis::InputType;
}

#endif  // THESIS_FILESYSTEM_HPP_97774A262F7D42D8B60B47FA7BFE3578
