#ifndef THESIS_ARGUMENT_PARSER_HPP_188B573712AC400495BF443E849980D4
#define THESIS_ARGUMENT_PARSER_HPP_188B573712AC400495BF443E849980D4

#include "quick_arg_parser.hpp"
#include "common.hpp"
#include "helper/filesystem.hpp"
#include <NamedType/named_type.hpp>

namespace QuickArgParserInternals {
    template<>
    struct ArgConverter<thesis::InputType, void> {
        static thesis::InputType makeDefault() {
            return thesis::InputType::Detect;
        }
        static thesis::InputType deserialise(const std::string &from) {
            if (from == "tud") {
                return thesis::InputType::TUD;
            }
            if (from == "konect") {
                return thesis::InputType::Konect;
            }
            throw ArgumentError("Invalid value for InputType argument: " + from);
        }

        static constexpr auto canDo = true;
    };

    template<typename T, typename Parameter, template<typename> class... Skills>
    struct ArgConverter<::fluent::NamedType<T, Parameter, Skills...>, void> {
        using arg_t = ::fluent::NamedType<T, Parameter, Skills...>;

        static arg_t makeDefault() {
            return {};
        }

        static arg_t deserialise(const std::string &from) {
            return arg_t{ArgConverter<T>::deserialise(from)};
        }

        static constexpr auto canDo = true;
    };
}  // namespace QuickArgParserInternals

namespace helper {
    struct ProgArgs : MainArguments<ProgArgs> {
        fs::path m_path = argument(0);

        bool m_transform_only = option("transform-only", 't', "Only transform changing labels.");
        thesis::time_t m_incubation_period =
            option("incubation_period", 'i', "Time until node becomes infectious. [Default = 0]") = thesis::time_t{};
        const thesis::time_t m_recovery_period =
            option("recovery-period", 'r', "Time until node recovers. [Default = infty]") = thesis::eternity;
        const bool m_dump_mdl = option("dump-mdl", 'd', "Output intermediate MDL.");
        const bool m_use_mdl = option("use-mdl", 'u', "Always use MDL to compute minimal times.");

        bool m_simple_kernel = option("simple-kernel", 's', "Compute simple kernel.");
        size_t m_simple_partitions = option("partitions", 'p', "Number of bins for simple kernel. [Default = 10]") = 10;
        bool m_simple_cumulative = option("cumulative", 'c', "Make simple kernel histogram cumulative.");

        thesis::InputType m_input_type = option("input-type") = thesis::InputType::Detect;
        fs::path m_output = option("output", 'o', "Output path.") = "";

        std::string dataset_name{};
    };

    auto get_parsed(int argc, char **argv) -> ProgArgs;
}  // namespace helper

#endif  // THESIS_ARGUMENT_PARSER_HPP_188B573712AC400495BF443E849980D4