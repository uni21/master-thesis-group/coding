#ifndef THESIS_FLAT_MAP_HPP_ABB43B1EF493429EAA2DD84623AB47B1
#define THESIS_FLAT_MAP_HPP_ABB43B1EF493429EAA2DD84623AB47B1
#include <vector>
#include <utility>
#include <algorithm>

namespace helper {
    template<class Key, class Value>
    using flat_map_t = std::vector<std::pair<Key, Value>>;

    template<class FlatMap, class Key>
    auto flat_find(FlatMap &&fmap, Key &&k) -> decltype(begin(fmap)) {
        return std::find_if(begin(fmap), end(fmap), [&](const auto &pair) { return pair.first == k; });
    }
    template<class FlatMap, class Key>
    auto flat_get(FlatMap &&fmap, Key &&k) -> decltype(begin(fmap)->second) {
        return flat_find(std::forward<FlatMap>(fmap), std::forward<Key>(k))->second;
    }

    template<class FlatMap, class Key, class Value>
    auto flat_set(FlatMap &fmap, Key &&k, Value &&v) -> decltype(fmap[0]) {
        return fmap.emplace_back(std::forward<Key>(k), std::forward<Value>(v));
    }
}  // namespace helper

#endif  // THESIS_FLAT_MAP_HPP_ABB43B1EF493429EAA2DD84623AB47B1
