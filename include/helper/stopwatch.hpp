#ifndef THESIS_STOPWATCH_HPP_3897148788074854886ADFD114ADD937
#define THESIS_STOPWATCH_HPP_3897148788074854886ADFD114ADD937

#include <chrono>
#include <string_view>
namespace helper {
    struct Stopwatch {
        using clock_t = std::chrono::high_resolution_clock;
        using duration_t = clock_t::duration;
        using time_point_t = clock_t::time_point;

        Stopwatch();

        auto lap() -> duration_t;
        auto print_lap(std::string_view text = "") -> duration_t;

        [[nodiscard]] auto total() const -> duration_t;
        auto print_total(std::string_view text = "") const -> duration_t;

      private:
        const time_point_t m_start;
        time_point_t m_last;
    };
}  // namespace helper

#endif  // THESIS_STOPWATCH_HPP_3897148788074854886ADFD114ADD937
