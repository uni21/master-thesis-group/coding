#ifndef THESIS_DEBUG_HPP_B5DA4AC21E38486595724A8557C40849
#define THESIS_DEBUG_HPP_B5DA4AC21E38486595724A8557C40849

#include "iostream"

namespace helper {
    template<class T>
    void print_template(T &&);

#ifndef NDEBUG
#    define DEBUG_PRINT(x)                                                                                             \
        do {                                                                                                           \
            std::cerr << __FILE__ << ':' << __LINE__ << ": " << x << '\n';                                             \
        } while (0)
#else
#    define DEBUG_PRINT(x)                                                                                             \
        do {                                                                                                           \
        } while (0)
#endif

}  // namespace helper

#endif  // THESIS_DEBUG_HPP_B5DA4AC21E38486595724A8557C40849
