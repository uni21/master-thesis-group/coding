#ifndef THESIS_DB_ACCESS_HPP_883FCDAF6FD54B77BFF07B03DA253077
#define THESIS_DB_ACCESS_HPP_883FCDAF6FD54B77BFF07B03DA253077

#include "graph/common_defs.hpp"
namespace helper {

    template<class GraphDB, class Func>
    void map_all_nodes(GraphDB &&db, Func &&f) {
        for (auto &graph: db) {
            for (auto handle: graph.nodes()) {
                f(handle);
            }
        }
    }

    template<class GraphDB, class Func>
    void map_all_edges(GraphDB &&db, Func &&f) {
        for (auto &graph: db) {
            for (auto handle: graph.edges()) {
                f(handle);
            }
        }
    }

}  // namespace helper

#endif  // THESIS_DB_ACCESS_HPP_883FCDAF6FD54B77BFF07B03DA253077
