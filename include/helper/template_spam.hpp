
#ifndef THESIS_TEMPLATE_SPAM_HPP_3C250D7C8A5842C8A50CB6D339FE771B
#define THESIS_TEMPLATE_SPAM_HPP_3C250D7C8A5842C8A50CB6D339FE771B

#include <tuple>
#include <type_traits>

namespace helper {
    template<class T>
    struct tuple_wrap {
        using type = std::tuple<T>;
    };
    template<class... Args>
    struct tuple_wrap<std::tuple<Args...>> {
        using type = std::tuple<Args...>;
    };

    template<class T, class...>
    using first = T;

#define FuncObj(x) [](auto &&...args) { return x(std::forward<decltype(args)>(args)...); }
}  // namespace helper
#endif  // THESIS_TEMPLATE_SPAM_HPP_3C250D7C8A5842C8A50CB6D339FE771B
