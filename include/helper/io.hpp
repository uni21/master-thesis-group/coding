#ifndef THESIS_IO_HPP_B111457685F44C4A94354F6FADF6ED3D
#define THESIS_IO_HPP_B111457685F44C4A94354F6FADF6ED3D

#include <fstream>
#include <string_view>
#include <tuple>
#include <vector>
#include "graph/common_defs.hpp"
#include "helper/template_spam.hpp"
#include "helper/filesystem.hpp"
#include "argument_parser.hpp"

namespace helper {
    using namespace std::string_view_literals;
    constexpr auto adjacency_appendix = "A"sv;
    constexpr auto graph_indicator_appendix = "graph_indicator"sv;
    constexpr auto graph_label_appendix = "graph_labels"sv;
    constexpr auto edge_attributes_appendix = "edge_attributes"sv;
    constexpr auto node_labels_appendix = "node_labels"sv;

    inline auto filename_generator(const fs::path &target) {
        return [&target](std::string_view appendix) {
            auto file_name = target;
            file_name += '_';
            file_name += appendix;
            file_name += ".txt";
            return file_name;
        };
    }
    template<class Tuple, size_t... indices>
    auto read_tuple_impl(std::ifstream &input, Tuple &out, std::index_sequence<indices...>) {
        return (input >> ... >> std::get<indices>(out)).good();
    }

    template<class Tuple>
    auto read_tuple(std::ifstream &input, Tuple &out) -> bool {
        return read_tuple_impl(input, out, std::make_index_sequence<std::tuple_size_v<Tuple>>{});
    }

    template<class... Args, class Func>
    auto read_to(const fs::path &filename, Func f) {
        static_assert(std::is_invocable_v<Func, Args...>);
        if (!exists(filename)) {
            throw std::runtime_error("File not found");
        }

        std::ifstream input{filename};
        std::tuple<Args...> buffer;

        while (read_tuple(input, buffer)) {
            std::apply(f, buffer);
        }
    }

    template<class... Tuples, class Func, size_t... Indices>
    auto read_parallel_impl(Func f, std::index_sequence<Indices...>,
                            helper::first<const fs::path &, Tuples>... file_names) {
        if (!(exists(file_names) && ...)) {
            throw std::runtime_error("File not found");
        }
        std::array files = {std::ifstream{file_names}...};
        std::tuple<typename helper::tuple_wrap<Tuples>::type...> buffers;

        while ((read_tuple(std::get<Indices>(files), std::get<Indices>(buffers)) && ...)) {
            auto buffer = std::tuple_cat(std::get<Indices>(buffers)...);
            std::apply(f, buffer);
        }
    }

    template<class... Tuples, class Func>
    auto read_parallel(Func f, helper::first<const fs::path &, Tuples>... file_names) {
        read_parallel_impl<Tuples...>(f, std::index_sequence_for<Tuples...>{}, file_names...);
    }

    template<class... Args, class Func>
    auto read_line_to(const fs::path &filename, Func f, char delim = ',') {
        using Arg =
            std::conditional_t<sizeof...(Args) == 1, std::tuple_element_t<0, std::tuple<Args...>>, std::tuple<Args...>>;

        static_assert(std::is_invocable_v<Func, std::vector<Arg>>);
        auto input = std::ifstream{filename};

        while (input) {
            std::vector<Arg> line_buffer;
            while (input.peek() != '\n') {
                line_buffer.emplace_back();
                const auto successful_read = [&]() {
                    if constexpr (sizeof...(Args) == 1) {
                        return static_cast<bool>(input >> line_buffer.back());
                    } else {
                        return read_tuple(input, line_buffer.back());
                    }
                }();

                if (!successful_read) {
                    return;
                }

                if (input.peek() == delim) {
                    input.get();
                }
            }
            input.get();
            f(std::move(line_buffer));
        }
    }

    template<class TupFirst, class... TupElems>
    auto write_delimited_vars(std::ostream &ostream, std::string_view delim, TupFirst &&first, TupElems &&...elems) {
        ostream << first;
        ((ostream << delim << elems), ...);
    }

    template<class Tuple>
    auto write_tuple(std::ostream &ostream, Tuple &&tup, std::string_view delim) {
        static_assert(std::tuple_size_v<std::decay_t<Tuple>> > 0, "Cannot write empty tuple.");
        std::apply([&](auto &&...args) { write_delimited_vars(ostream, delim, std::forward<decltype(args)>(args)...); },
                   tup);
    }

    // Konect uses unix times with dot, it is a pain...
    auto read_unix_time(std::istream &inp) -> std::chrono::nanoseconds;

    auto open_write(const fs::path &target) -> std::ofstream;

    auto get_output_path(const ProgArgs &, std::string_view suffix = "", bool force_suffix = false)
        -> std::pair<fs::path, bool>;
}  // namespace helper

namespace fluent {
    template<graph::GraphMemberKind kind>
    inline std::istream &operator>>(std::istream &istream, graph::id_t<kind> &id) {
        using id_t = graph::id_t<kind>;
        typename id_t::UnderlyingType tmp;
        istream >> tmp;
        id = id_t{tmp};
        return istream;
    }
}  // namespace fluent
#endif  // THESIS_IO_HPP_B111457685F44C4A94354F6FADF6ED3D