#ifndef THESIS_MISC_HPP_874D990B32F644328718E8E563DAA76B
#define THESIS_MISC_HPP_874D990B32F644328718E8E563DAA76B

#include <algorithm>
#include <random>
#include "NamedType/underlying_functionalities.hpp"

namespace helper {
    template<class T, class U>
    auto update_max(T &t, U &&u) -> T & {
        if (u > t) {
            t = std::forward<U>(u);
        }
        return t;
    }

    template<class T, class U>
    auto update_min(T &t, U &&u) -> T & {
        if (u < t) {
            t = std::forward<U>(u);
        }
        return t;
    }

    template<class Index, class Func>
    void repeat_n(Index n, Func &&f) {
        for (Index i{}; i != n; i = i + Index{1}) {
            if constexpr (std::is_invocable_v<Func, Index>) {
                f(i);
            } else {
                f();
            }
        }
    }

    inline auto get_mersenne_twister() {
        return std::mt19937_64{std::random_device{}()};
    }

    // GCC has a bug thinking the operator++()/operator++(int) are ambiguous. This is a workaround to this compiles with
    // gcc.
    template<class NT>
    auto preinc(NT &nt) {
        return ++static_cast<fluent::PreIncrementable<NT> &>(nt);
    }

    template<class NT>
    using numeric_limits = std::numeric_limits<typename NT::UnderlyingType>;
}  // namespace helper

#endif  // THESIS_MISC_HPP_874D990B32F644328718E8E563DAA76B
