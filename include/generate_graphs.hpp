#ifndef THESIS_GENERATE_GRAPHS_HPP_9CFED305F898469A8B0D534A8B6B5E66
#define THESIS_GENERATE_GRAPHS_HPP_9CFED305F898469A8B0D534A8B6B5E66

#include "tud/common.hpp"
#include "helper/misc.hpp"

namespace thesis {
    template<class Graph, class TimeGen, class DegreeGen, class NeighborGen>
    void generate_graph(Graph &g, TimeGen &&time_gen, DegreeGen &&degree_gen, NeighborGen &&neighbor_gen) {
        for (tud::time_t current{}; time_gen(current); helper::preinc(current)) {
            for (const auto node: g.nodes()) {
                const auto degree = degree_gen(g, current, node);

                helper::repeat_n(degree, [&]() { neighbor_gen(g, current, node, degree); });
            }
        }
    }

    auto random_static_graph_db(size_t num_graphs, size_t num_nodes, size_t avg_degree) -> tud::input_db_t;

}  // namespace thesis

#endif  // THESIS_GENERATE_GRAPHS_HPP_9CFED305F898469A8B0D534A8B6B5E66
