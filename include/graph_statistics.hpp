#ifndef THESIS_GRAPH_STATISTICS_HPP_149001E1C34944FCADF7FCB109E54516
#define THESIS_GRAPH_STATISTICS_HPP_149001E1C34944FCADF7FCB109E54516

#include "helper/misc.hpp"
#include <graph/common_defs.hpp>

namespace thesis {

    template<class Value>
    struct StatisticalInfo {
        Value min{helper::numeric_limits<Value>::max()};
        Value max;
        Value sum;

        [[nodiscard]] auto average(size_t number) const -> double {
            return static_cast<double>(sum.get()) / static_cast<double>(number);
        }
        void update(Value val) {
            helper::update_min(min, val);
            helper::update_max(max, val);
            sum += val;
        }
    };

    struct Statistics {
        size_t number_graphs{};
        StatisticalInfo<graph::node_id_t> node_stats{};
        StatisticalInfo<graph::edge_id_t> edge_stats{};
        StatisticalInfo<graph::node_id_t> degree_stats{};
    };

    static constexpr auto cell_width = 12;
    static constexpr auto headers = {"", "Min", "Max", "Sum", "Avg"};
    inline std::ostream &operator<<(std::ostream &out, const Statistics &stats) {
        auto print_cell = [&](const auto &val) { out << std::setw(cell_width) << val << " | "; };
        auto print_line = [&](const auto &name, const auto &statinfo, const auto divisor) {
            print_cell(name);
            print_cell(statinfo.min);
            print_cell(statinfo.max);
            print_cell(statinfo.sum);
            print_cell(statinfo.average(divisor));
            out << '\n';
        };

        out << std::left;
        out << "DB has " << stats.number_graphs << " graphs.\n";
        for (auto header: headers) {
            print_cell(header);
        }
        out << '\n';
        print_line("Nodes", stats.node_stats, stats.number_graphs);
        print_line("Edges", stats.edge_stats, stats.number_graphs);
        print_line("Degrees", stats.degree_stats, stats.node_stats.sum.get());

        return out;
    }

    template<class Graph>
    void update_stats(Statistics &stats, const Graph &g) {
        stats.node_stats.update(g.num_nodes());
        stats.edge_stats.update(g.num_edges());
        for (const auto &n: g.nodes()) {
            stats.degree_stats.update(n->degree());
        }
    }

    template<class GraphDB>
    auto get_statistics(const GraphDB &db) {
        Statistics stats{db.size()};

        for (auto graph: db) {
            update_stats(stats, graph);
        }
        return stats;
    }
}  // namespace thesis

#endif  // THESIS_GRAPH_STATISTICS_HPP_149001E1C34944FCADF7FCB109E54516
