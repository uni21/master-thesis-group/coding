#ifndef THESIS_MDL_HPP_50B255911C2F48AB8B29ADACA9C08BF1
#define THESIS_MDL_HPP_50B255911C2F48AB8B29ADACA9C08BF1

#include "tud/common.hpp"
namespace thesis {
    using underlying_t = time_t::UnderlyingType;
    using mdl_db_t = tud::GraphDB<int, underlying_t, underlying_t>;

    auto construct_mdl(const tud::transformed_db_t &, time_t incubation_period, time_t recovery_period) -> mdl_db_t;

    void longest_paths(mdl_db_t &);

    void map_back_times(mdl_db_t &, tud::transformed_db_t &);
}  // namespace thesis

#endif  // THESIS_MDL_HPP_50B255911C2F48AB8B29ADACA9C08BF1
