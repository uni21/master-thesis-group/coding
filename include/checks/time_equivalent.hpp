#ifndef THESIS_TIME_EQUIVALENT_HPP_49F4FBAFE84A4D4897237BF1B3FB3AEF
#define THESIS_TIME_EQUIVALENT_HPP_49F4FBAFE84A4D4897237BF1B3FB3AEF

#include "../tud/common.hpp"
#include "../tud/graph_db.hpp"

namespace thesis::checks {
    using graph_t = tud::input_db_t::graph_t;
    [[nodiscard]] bool check_time_equivalent(const graph_t &original, const graph_t &minimized);

    [[nodiscard]] auto get_time_diff(const graph_t &original, const graph_t &minimized,
                                     tud::time_t original_global_min = {}) -> tud::time_t;
}  // namespace thesis::checks

#endif  // THESIS_TIME_EQUIVALENT_HPP_49F4FBAFE84A4D4897237BF1B3FB3AEF
