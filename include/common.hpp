#ifndef THESIS_COMMON_HPP_F6853B4656324F48B7CD0732F0A13623
#define THESIS_COMMON_HPP_F6853B4656324F48B7CD0732F0A13623

#include "helper/misc.hpp"
#include <NamedType/named_type.hpp>
namespace thesis {
    enum class InputType { Detect, TUD, Konect };

    using time_t = fluent::NamedType<long long, struct _time_t, fluent::Arithmetic>;
    using dist_t = fluent::NamedType<long long, struct _dist_t, fluent::Arithmetic>;

    static constexpr time_t eternity{helper::numeric_limits<time_t>::max()};

    inline namespace literals {
        constexpr auto operator"" _t(unsigned long long t) -> time_t {
            return time_t{static_cast<long long>(t)};
        }
    }  // namespace literals
}  // namespace thesis
#endif  // THESIS_COMMON_HPP_F6853B4656324F48B7CD0732F0A13623
