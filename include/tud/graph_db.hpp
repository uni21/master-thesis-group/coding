#ifndef THESIS_GRAPH_DB_HPP_DA5E34815ED940AF821513AF7DD71D17
#define THESIS_GRAPH_DB_HPP_DA5E34815ED940AF821513AF7DD71D17

#include "graph/graph.hpp"
#include <vector>
namespace tud {
    template<graph::GraphMemberKind kind>
    struct GlobalIndex {
        std::size_t graph_index;
        graph::id_t<kind> member_index;
    };

    using global_node_index_t = GlobalIndex<graph::GraphMemberKind::e_Node>;
    using global_edge_index_t = GlobalIndex<graph::GraphMemberKind::e_Edge>;

    template<class GraphLabel, class NodeLabel = void, class EdgeLabel = void>
    struct GraphDB {
        using graph_t = graph::Graph<NodeLabel, EdgeLabel>;
        template<graph::GraphMemberKind kind>
        using handle_t = typename graph_t::template handle_t<kind>;
        template<graph::GraphMemberKind kind>
        using const_handle_t = typename graph_t::template const_handle_t<kind>;
        using node_label_t = typename graph_t::node_label_t;
        using edge_label_t = typename graph_t::edge_label_t;
        using graph_label_t = GraphLabel;
        using node_handle_t = typename graph_t::node_handle_t;
        using edge_handle_t = typename graph_t::edge_handle_t;
        using index_t = std::size_t;

        template<graph::GraphMemberKind kind>
        auto operator[](const GlobalIndex<kind> &index) -> handle_t<kind> {
            return m_graphs[index.graph_index][index.member_index];
        }

        template<graph::GraphMemberKind kind>
        auto operator[](const GlobalIndex<kind> &index) const -> const_handle_t<kind> {
            return m_graphs[index.graph_index][index.member_index];
        }

        auto operator[](index_t index) -> graph_t & {
            return m_graphs[index];
        }

        auto operator[](index_t index) const -> const graph_t & {
            return m_graphs[index];
        }

        auto label(index_t index) const -> graph_label_t {
            return m_labels[index];
        }

        template<class... Args>
        auto add_graph(graph_label_t label, Args &&...args) -> index_t {
            m_graphs.emplace_back(std::forward<Args>(args)...);
            m_labels.push_back(label);
            return m_graphs.size() - 1;
        }

        void reserve(index_t len) {
            m_graphs.reserve(len);
            m_labels.reserve(len);
        }

        [[nodiscard]] auto size() const -> size_t {
            return m_graphs.size();
        }

        auto begin() const {
            return std::begin(m_graphs);
        }

        auto end() const {
            return std::end(m_graphs);
        }

        auto begin() {
            return std::begin(m_graphs);
        }

        auto end() {
            return std::end(m_graphs);
        }

      private:
        std::vector<graph_t> m_graphs;
        std::vector<graph_label_t> m_labels;
    };
}  // namespace tud

#endif  // THESIS_GRAPH_DB_HPP_DA5E34815ED940AF821513AF7DD71D17
