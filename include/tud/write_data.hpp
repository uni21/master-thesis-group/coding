#ifndef THESIS_WRITE_DATA_HPP_0F006EAC713344FDA502FC518D2AB182
#define THESIS_WRITE_DATA_HPP_0F006EAC713344FDA502FC518D2AB182

#include <iosfwd>
#include "./common.hpp"
#include "mdl.hpp"

namespace tud {
    void write_dataset(const fs::path &target, const input_db_t &graph_db);
    void write_dataset(const fs::path &target, const transformed_db_t &graph_db);
    void write_dataset(const fs::path &target, const ::thesis::mdl_db_t &db);

    void write_gram(const gram_t &gram, const input_db_t &db, std::ostream &ostream);

    void write_feature_maps(const std::vector<feature_t> &fmap, std::ostream &ostream);

    void write_csv(std::string_view dataset_name, const fs::path &target, const transformed_db_t &graph_db);
}  // namespace tud

#endif  // THESIS_WRITE_DATA_HPP_0F006EAC713344FDA502FC518D2AB182
