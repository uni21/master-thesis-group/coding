
#ifndef THESIS_READ_DATA_HPP_C21A5446D82A4805B71D962FD0FABA0C
#define THESIS_READ_DATA_HPP_C21A5446D82A4805B71D962FD0FABA0C

#include "common.hpp"
#include "NamedType/named_type.hpp"
#include <string_view>
#include <filesystem>

namespace tud {
    auto read_dataset(const fs::path &directory) -> input_db_t;
}  // namespace tud
#endif  // THESIS_READ_DATA_HPP_C21A5446D82A4805B71D962FD0FABA0C
