#ifndef THESIS_TRANSFORMATIONS_HPP_F3610AD55AC1450B87E1C0F933D3A0F0
#define THESIS_TRANSFORMATIONS_HPP_F3610AD55AC1450B87E1C0F933D3A0F0

#include "tud/common.hpp"

namespace tud {
    void shift_times_to_null(transformed_db_t &);
    void globally_minimize_times(transformed_db_t &);
    auto replace_temporal_nodes(input_db_t &&input, time_t incubation_period = {}) -> transformed_db_t;
    auto full_transform(input_db_t &&input, time_t incubation_period = {}) -> transformed_db_t;
}  // namespace tud

#endif  // THESIS_TRANSFORMATIONS_HPP_F3610AD55AC1450B87E1C0F933D3A0F0
