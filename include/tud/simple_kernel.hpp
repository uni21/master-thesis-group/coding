#ifndef THESIS_SIMPLE_KERNEL_HPP_D7D27FD4C81E44C985C537476B95A05B
#define THESIS_SIMPLE_KERNEL_HPP_D7D27FD4C81E44C985C537476B95A05B

#include <vector>
#include "tud/common.hpp"

namespace tud {
    auto infectiousness_histogram(const input_db_t &db, size_t partitions, bool cumulative = false)
        -> std::vector<feature_t>;

    auto calculate_gram(const std::vector<feature_t> &feature_map) -> gram_t;
}  // namespace tud

#endif  // THESIS_SIMPLE_KERNEL_HPP_D7D27FD4C81E44C985C537476B95A05B
