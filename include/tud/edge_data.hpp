#ifndef THESIS_EDGE_DATA_HPP_483324D1ED9049D08C8CE7C424580DBC
#define THESIS_EDGE_DATA_HPP_483324D1ED9049D08C8CE7C424580DBC

#include "tud/common.hpp"
namespace tud {
    struct EdgeData {
        // Annoyingly, aggregate construction does not work with emplace_back...
        explicit EdgeData(time_t time_ = {}, dist_t length_ = dist_t{1}) : time(time_), length(length_) {}
        time_t time;
        dist_t length;
    };
}  // namespace tud

#endif  // THESIS_EDGE_DATA_HPP_483324D1ED9049D08C8CE7C424580DBC
