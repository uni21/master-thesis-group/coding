#ifndef THESIS_TEMPORAL_NODE_HPP_1B9AA96F2FD94D1E8F4F888E85C4689D
#define THESIS_TEMPORAL_NODE_HPP_1B9AA96F2FD94D1E8F4F888E85C4689D

#include "common.hpp"

namespace tud {
    template<class Label, class Time>
    struct LabelChange {
        Time m_time;
        Label m_label;
    };

    template<class Label, class Time>
    struct TemporalNode {
        using time_t = Time;
        using label_t = Label;
        using label_change_t = LabelChange<label_t, time_t>;

        TemporalNode() = default;
        explicit TemporalNode(std::vector<label_change_t> &&labels) : m_labels(std::move(labels)) {}

        void add_label(time_t time, label_t label) {
            auto pos = pos_from_time(time);

            if (pos->m_time == time) {
                pos->m_label = label;
            } else {
                m_labels.insert(pos, LabelChange{time, label});
            }
        }

        void add_label(const label_change_t &change) {
            add_label(change.m_time, change.m_label);
        }

        void set_labels(std::vector<label_change_t> &&new_labels) {
            m_labels = std::move(new_labels);
        }

        [[nodiscard]] auto changes_at(const time_t &t) const -> bool {
            const auto at_t = pos_from_time(t);
            return at_t != end(m_labels) && at_t->m_time == t;
        }

        [[nodiscard]] auto by_time(const time_t &t) -> label_change_t & {
            const auto pos = pos_from_time(t);
            if (pos == begin(m_labels)) {
                return m_labels.front();
            }
            return *std::prev(pos_from_time(t));
        }

        [[nodiscard]] auto by_time(const time_t &t) const -> const label_change_t & {
            const auto pos = pos_from_time(t);
            if (pos == begin(m_labels)) {
                return m_labels.front();
            }
            return *std::prev(pos);
        }

        [[nodiscard]] auto by_index(const size_t index) -> label_change_t & {
            return m_labels[index];
        }

        [[nodiscard]] auto by_index(const size_t index) const -> const label_change_t & {
            return m_labels[index];
        }

        [[nodiscard]] auto first_index_after(const time_t &t) const -> size_t {
            auto pos = pos_from_time<std::less_equal<time_t>>(t);
            return static_cast<size_t>(std::distance(begin(m_labels), pos));
        }

        [[nodiscard]] auto all() const {
            return nano::ref_view{m_labels};
        }

        [[nodiscard]] auto all() {
            return nano::ref_view{m_labels};
        }

        [[nodiscard]] auto num_changes() const -> size_t {
            return m_labels.size();
        }

        [[nodiscard]] auto changes() const -> bool {
            return m_labels.size() > 1;
        }

        template<class TLabel, class TTime>
        friend std::ostream &operator<<(std::ostream &, const TemporalNode<TLabel, TTime> &);

        void reset() {
            m_labels.erase(std::next(m_labels.begin()), m_labels.end());
        }

      private:
        template<class Comp = std::less<time_t>>
        auto pos_from_time(const time_t &t, Comp cmp = {}) const {
            return std::lower_bound(
                begin(m_labels), end(m_labels), t, [cmp](const label_change_t &change, const time_t time) {
                    return cmp(change.m_time, time);
                });
        }

        template<class Comp = std::less<time_t>>
        auto pos_from_time(const time_t &t, Comp cmp = {}) {
            return std::lower_bound(
                begin(m_labels), end(m_labels), t, [cmp](const label_change_t &change, const time_t time) {
                    return cmp(change.m_time, time);
                });
        }
        std::vector<label_change_t> m_labels;
    };

    template<class Label, class Time>
    std::ostream &operator<<(std::ostream &out, const TemporalNode<Label, Time> &node) {
        auto first = true;
        for (const auto &label: node.m_labels) {
            if (first) {
                first = false;
            } else {
                out << ", ";
            }
            out << label.m_time << ", " << label.m_label;
        }
        return out;
    }

    template<class Label, class Time>
    std::istream &operator>>(std::istream &in, LabelChange<Label, Time> &change) {
        in >> change.m_time.get();
        [[maybe_unused]] const char delim = static_cast<char>(in.get());
        assert(!in || delim == ',');
        in >> change.m_label;
        return in;
    }

}  // namespace tud

#endif  // THESIS_TEMPORAL_NODE_HPP_1B9AA96F2FD94D1E8F4F888E85C4689D
