#ifndef THESIS_ALL_DEFS_HPP_50D97EFAC807459C86DEB7132B6C93FC
#define THESIS_ALL_DEFS_HPP_50D97EFAC807459C86DEB7132B6C93FC

// A header that includes all complete definitions.

#include "tud/graph_db.hpp"
#include "tud/edge_data.hpp"
#include "tud/temporal_node.hpp"

#endif  // THESIS_ALL_DEFS_HPP_50D97EFAC807459C86DEB7132B6C93FC
