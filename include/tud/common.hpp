#ifndef THESIS_COMMON_HPP_EC600EB3533C4DDFA62B0F2300233B37
#define THESIS_COMMON_HPP_EC600EB3533C4DDFA62B0F2300233B37

#include <string_view>
#include "common.hpp"
#include "helper/filesystem.hpp"

namespace tud {
    using thesis::time_t;
    using thesis::dist_t;

    using node_label_t = long long;
    template<class = node_label_t, class = thesis::time_t>
    struct TemporalNode;
    struct EdgeData;
    template<class, class, class>
    struct GraphDB;
    using input_db_t = GraphDB<int, TemporalNode<>, EdgeData>;
    using transformed_db_t = GraphDB<int, node_label_t, EdgeData>;

    using feature_t = std::vector<size_t>;
    using gram_t = std::vector<std::vector<size_t>>;

    struct Kernel {
        std::vector<feature_t> m_fmaps;
        gram_t m_gram;
        const input_db_t &m_db;
    };
}  // namespace tud

#endif  // THESIS_COMMON_HPP_EC600EB3533C4DDFA62B0F2300233B37
