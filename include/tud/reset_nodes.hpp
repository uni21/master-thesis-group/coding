#ifndef THESIS_RESET_NODES_HPP_3C617987701747D49AA3D53DF59F871B
#define THESIS_RESET_NODES_HPP_3C617987701747D49AA3D53DF59F871B

#include "common.hpp"
#include "graph_db.hpp"
#include "temporal_node.hpp"
#include "edge_data.hpp"

namespace tud {
    template<class Decider>
    void reset_nodes(input_db_t &db, Decider &&decider) {
        for (auto &graph: db) {
            for (auto node: graph.nodes()) {
                if (decider(node)) {
                    node->label().reset();
                }
            }
        }
    }

    void reset_nodes_equidistributed(input_db_t &db, double reset_probability) {
        std::random_device rd;
        std::mt19937 engine(rd());
        std::bernoulli_distribution dist(reset_probability);

        reset_nodes(db, [&](const auto &node) { return node->label().changes() && dist(engine); });
    }
}  // namespace tud

#endif  // THESIS_RESET_NODES_HPP_3C617987701747D49AA3D53DF59F871B
