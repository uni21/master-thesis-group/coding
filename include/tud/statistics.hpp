#ifndef THESIS_STATISTICS_HPP_F4FDDE6214BE4A029B512C99A7A31FFC
#define THESIS_STATISTICS_HPP_F4FDDE6214BE4A029B512C99A7A31FFC

#include <cassert>
#include <numeric>
#include "common.hpp"
#include "graph_db.hpp"
#include "graph/nanorange.hpp"

namespace tud {
    template<class T>
    using by_label_t = std::array<T, 2>;

    struct infectiousness {
        size_t infections{};
        size_t encounters{};

        [[nodiscard]] auto probability() const -> double {
            return encounters == 0 ? 0.0 : static_cast<double>(infections) / static_cast<double>(encounters);
        }

        auto operator+=(const infectiousness &other) -> infectiousness & {
            infections += other.infections;
            encounters += other.encounters;
            return *this;
        }
    };

    inline auto operator+(infectiousness lhs, const infectiousness &rhs) -> infectiousness {
        return lhs += rhs;
    }

    auto get_infections_of_node(input_db_t::graph_t::const_node_handle_t handle) -> infectiousness;
    auto get_infections(const input_db_t &) -> by_label_t<std::vector<std::vector<time_t>>>;
    auto get_infectiousness(const input_db_t &) -> by_label_t<std::vector<infectiousness>>;

    template<class R, class BinOp = std::plus<>>
    inline auto average(const R &range, BinOp op = {}) {
        using val_t = typename R::value_type;
        const auto sum =
            std::accumulate(begin(range), end(range), std::pair<val_t, size_t>{}, [&](auto &&lhs, auto &&rhs) {
                return std::pair<val_t, size_t>{op(lhs.first, std::forward<decltype(rhs)>(rhs)), lhs.second + 1};
            });
        return sum.second ? sum.first / val_t{sum.second} : 0;
    }

    template<class R, class Cmp = std::less<>>
    inline auto mean(R &range, Cmp cmp = {}) {
        const auto mean_point = std::next(begin(range), nano::size(range) / 2);
        nano::nth_element(range, mean_point, cmp);
        return *mean_point;
    }

    template<class R, class F>
    inline auto reduce(by_label_t<R> &range, F &&func) {
        using ret_t = std::invoke_result_t<F, typename R::value_type &>;
        by_label_t<std::vector<ret_t>> ret;

        for (const auto label: {0ul, 1ul}) {
            ret[label].reserve(nano::size(range[label]));
            for (auto &elem: range[label]) {
                ret[label].push_back(func(elem));
            }
        }
        return ret;
    }
}  // namespace tud

#endif  // THESIS_STATISTICS_HPP_F4FDDE6214BE4A029B512C99A7A31FFC
