#ifndef THESIS_FUNCTIONALITIES_HPP_D460EEEE92604836970821925D105389
#define THESIS_FUNCTIONALITIES_HPP_D460EEEE92604836970821925D105389

#include <helper/stopwatch.hpp>
#include "helper/argument_parser.hpp"
#include "tud/common.hpp"

namespace thesis {
    auto read_to_tud_input(const helper::ProgArgs &) -> tud::input_db_t;
    auto read_to_transformed(const helper::ProgArgs &args) -> tud::transformed_db_t;

    void calculate_minimal_times(const helper::ProgArgs &, tud::transformed_db_t &, helper::Stopwatch &);

    void dump_transformed(const helper::ProgArgs &args, const tud::transformed_db_t &);

    auto calc_simple_kernel(const helper::ProgArgs &args, const tud::input_db_t &) -> tud::Kernel;
    void dump_simple_kernel(const helper::ProgArgs &args, const tud::Kernel &);
}  // namespace thesis

#endif  // THESIS_FUNCTIONALITIES_HPP_D460EEEE92604836970821925D105389
